window.onload = function() {
    for (let i = 1; i <= 100; i++){
        let element = document.createElement('li');
        element.innerText = "";
        if((i % 3 === 0) && (i % 5 === 0 )) {element.innerText += "FizzBuzz"; element.classList.add('fizzBuzzList__list--fizzBuzz')}
        else { 
            if(i % 3 === 0 ) {element.innerText += "Fizz"; element.classList.add('fizzBuzzList__list--fizz')}
            if(i % 5 === 0 ) {element.innerText += "Buzz"; element.classList.add('fizzBuzzList__list--buzz')}
            if(element.innerText === ""){element.innerText = i; element.classList.add('fizzBuzzList__list--number')}
        }
        document.getElementById('fizzBuzzList__list').appendChild(element);
        //document.getElementById('fizzBuzzList__list').appendChild(document.createElement('li'));
    }
}
